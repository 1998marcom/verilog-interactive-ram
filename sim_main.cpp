#include "Vcusu.h"
#include "verilated.h"
#include <cstdio>

int main(int argc, char** argv, char** env) {
	Verilated::commandArgs(argc, argv);
	Vcusu* top = new Vcusu;
	//while (!Verilated::gotFinish()) { top->eval(); }
	unsigned int address, data;
	printf("Usage: \n\
\tR <address> to read address, where address is an hex number < 256\n\
\tW <address> to write address, where address is an hex number < 256\n");
	while (true) {
		top->clk = 0;
		top->en = 0;
		top->we = 0;
		top->eval();
		char control;
		printf("\nType your control: ");
		if (scanf(" %c %x", &control, &address)!= 2)
			exit(42);
		top->en = 1;
		top->address = address;
		if (control=='R') {
			top->we = 0;
			top->clk = 1;
			top->eval();
			data = top->data;
			printf("Data read: 0x%x at address 0x%x\n", data, address);
		} else if (control=='W') {
			printf("Type your hex data: ");
			scanf(" %x", &data);
			top->we = 1;
			top->clk = 1;
			top->data = data;
			top->eval();
			printf("Written data 0x%x to address 0x%x\n", data, address);
		} else 
			exit(43);
	}
	delete top;
	exit(0);
}
