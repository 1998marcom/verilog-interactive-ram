# Verilog Interactive RAM

Simple project for personal future reference: a complete setup of an interactive RAM written in verilog, compiled with verilator, and using its bindings to have a c++ coded terminal wrapper of the storage. Useless but funny.

## How to run 

Requirements: `make` and `verilator`.

```
./compile.sh
./obj_dir/Vcusu
```