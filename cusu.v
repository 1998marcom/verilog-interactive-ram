//-----------------------------------------------------
// This is my first Verilog Program
// Design Name : hello_world
// File Name : hello_world.v
// Function : This program will print 'hello world'
// Coder    : Deepak
//-----------------------------------------------------
module cusu #(parameter wordsize=8, addrsize=8)
  (
  input wire [addrsize-1:0] address,
  inout wire [wordsize-1:0] data,
  input wire clk, 
  input wire en,
  input wire we
  );
  
  reg [wordsize-1:0] mem [0:(1<<addrsize)-1];

  always @ (posedge clk) begin
	if (en & we) begin
	  mem[address] <= data;
	end
	else if (en & ~we) begin
      data = mem[address];
	end
  end

endmodule // End of Module hello_world

